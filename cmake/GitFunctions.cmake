# This module adds helpers functions for Git

macro(git_describe OUTPUT_VARIABLE DESCRIBE_ARGUMENTS)
    execute_process(
        COMMAND git describe ${DESCRIBE_ARGUMENTS}
        WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
        OUTPUT_VARIABLE ${OUTPUT_VARIABLE}
        OUTPUT_STRIP_TRAILING_WHITESPACE
        )
endmacro()