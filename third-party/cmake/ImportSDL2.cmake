# This module allows to import sdl2 from third-party folder

FetchContent_Declare(
        sdl2
        GIT_REPOSITORY  https://github.com/libsdl-org/SDL.git
        GIT_TAG         release-2.30.8
)

FetchContent_MakeAvailable(sdl2)