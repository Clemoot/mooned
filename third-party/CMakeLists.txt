cmake_minimum_required(VERSION 3.16)

include(FetchContent)

include("${CMAKE_CURRENT_LIST_DIR}/cmake/ImportGoogleTest.cmake")
include("${CMAKE_CURRENT_LIST_DIR}/cmake/ImportSDL2.cmake")