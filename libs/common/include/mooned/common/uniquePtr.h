#pragma once

namespace Mooned
{
template <typename T>
class UniquePtr
{
    template <typename U>
    friend class UniquePtr;

public:
    using element_type_t = T;

    constexpr UniquePtr() noexcept;
    constexpr UniquePtr(T* ptr) noexcept;
    constexpr UniquePtr(std::nullptr_t) noexcept;
    template <typename Derived, typename = std::enable_if_t<std::is_base_of_v<T, Derived>, int>>
    constexpr UniquePtr(Derived* ptr) noexcept;
    constexpr UniquePtr(UniquePtr<T>&& other) noexcept;
    template <typename Derived, typename = std::enable_if_t<std::is_base_of_v<T, Derived>, int>>
    constexpr UniquePtr(UniquePtr<Derived>&& other) noexcept;
    constexpr UniquePtr(const UniquePtr<T>& other) noexcept = delete;
    ~UniquePtr();

    T* get() noexcept;
    const T* get() const noexcept;
    T* release() noexcept;
    void reset(T* ptr = nullptr) noexcept;
    void reset(std::nullptr_t) noexcept;

    T& operator*() { return *m_Ptr; }
    const T& operator*() const { return *m_Ptr; }
    T* operator->() { return m_Ptr; }
    const T* operator->() const { return m_Ptr; }

    operator bool() const noexcept { return m_Ptr != nullptr; }

    UniquePtr<T>& operator=(UniquePtr<T>&& other) noexcept;
    template <typename Derived, typename = std::enable_if_t<std::is_base_of_v<T, Derived>, int>>
    UniquePtr<T>& operator=(UniquePtr<Derived>&& other) noexcept;
    UniquePtr<T>& operator=(const UniquePtr<T>& other) noexcept = delete;
    UniquePtr<T>& operator=(std::nullptr_t) noexcept;

private:
    T* m_Ptr;
};

template <
    typename T,
    typename... ConstructorArgs,
    typename = std::enable_if_t<std::is_constructible_v<T, ConstructorArgs...>, int>>
UniquePtr<T> make_unique(ConstructorArgs&&... args) noexcept
{
    return UniquePtr<T>(new T(args...));
}

template <typename Derived, typename Base, typename = std::enable_if_t<std::is_base_of_v<Base, Derived>, int>>
UniquePtr<Derived>& dynamic_cast_pointer(UniquePtr<Base>& ptr) noexcept
{
    Derived* new_ptr = dynamic_cast<Derived*>(ptr.get());
    if (!new_ptr) {
        static UniquePtr<Derived> null(nullptr);
        return null;
    }
    return *reinterpret_cast<UniquePtr<Derived>*>(&ptr);
}

template <typename Derived, typename Base, typename = std::enable_if_t<std::is_base_of_v<Base, Derived>, int>>
const UniquePtr<Derived>& dynamic_cast_pointer(const UniquePtr<Base>& ptr) noexcept
{
    const Derived* new_ptr = dynamic_cast<const Derived*>(ptr.get());
    if (!new_ptr) {
        static UniquePtr<Derived> null(nullptr);
        return null;
    }
    return *reinterpret_cast<const UniquePtr<Derived>*>(&ptr);
}

// ----------------

template <typename T>
constexpr UniquePtr<T>::UniquePtr() noexcept: m_Ptr(nullptr)
{
}

template <typename T>
constexpr UniquePtr<T>::UniquePtr(T* ptr) noexcept: m_Ptr(ptr)
{
}

template <typename T>
constexpr UniquePtr<T>::UniquePtr(std::nullptr_t) noexcept: m_Ptr(nullptr)
{
}

template <typename T>
template <typename Derived, typename>
constexpr UniquePtr<T>::UniquePtr(Derived* ptr) noexcept: m_Ptr(ptr)
{
}

template <typename T>
constexpr UniquePtr<T>::UniquePtr(UniquePtr<T>&& other) noexcept: m_Ptr(other.m_Ptr)
{
    other.m_Ptr = nullptr;
}

template <typename T>
template <typename Derived, typename>
constexpr UniquePtr<T>::UniquePtr(UniquePtr<Derived>&& other) noexcept: m_Ptr(other.m_Ptr)
{
    other.m_Ptr = nullptr;
}

template <typename T>
UniquePtr<T>::~UniquePtr()
{
    if (m_Ptr)
        delete m_Ptr;
}

template <typename T>
T* UniquePtr<T>::get() noexcept
{
    return m_Ptr;
}

template <typename T>
const T* UniquePtr<T>::get() const noexcept
{
    return m_Ptr;
}

template <typename T>
void UniquePtr<T>::reset(T* ptr) noexcept
{
    if (m_Ptr)
        delete m_Ptr;
    m_Ptr = ptr;
}

template <typename T>
void UniquePtr<T>::reset(std::nullptr_t) noexcept
{
    delete m_Ptr;
    m_Ptr = nullptr;
}

template <typename T>
T* UniquePtr<T>::release() noexcept
{
    T* ptr = m_Ptr;
    m_Ptr  = nullptr;
    return ptr;
}

template <typename T>
UniquePtr<T>& UniquePtr<T>::operator=(UniquePtr<T>&& other) noexcept
{
    reset(other.m_Ptr);
    other.m_Ptr = nullptr;
    return *this;
}

template <typename T>
template <typename Derived, typename>
UniquePtr<T>& UniquePtr<T>::operator=(UniquePtr<Derived>&& other) noexcept
{
    reset(other.m_Ptr);
    other.m_Ptr = nullptr;
    return *this;
}

template <typename T>
UniquePtr<T>& UniquePtr<T>::operator=(std::nullptr_t) noexcept
{
    reset(nullptr);
    return *this;
}

template <typename T, typename U>
bool operator==(const UniquePtr<T>& a, const UniquePtr<U>& b) noexcept
{
    return a.get() == b.get();
}

template <typename T, typename U>
bool operator==(const T* const a, const UniquePtr<U>& b) noexcept
{
    return a == b.get();
}

template <typename T, typename U>
bool operator==(const UniquePtr<T>& a, const U* const b) noexcept
{
    return a.get() == b;
}

template <typename T>
bool operator==(const UniquePtr<T>& ptr, std::nullptr_t) noexcept
{
    return ptr.get() == nullptr;
}

template <typename T>
bool operator==(std::nullptr_t, const UniquePtr<T>& ptr) noexcept
{
    return ptr.get() == nullptr;
}

template <typename T, typename U>
bool operator!=(const UniquePtr<T>& a, const UniquePtr<U>& b) noexcept
{
    return a.get() != b.get();
}

template <typename T, typename U>
bool operator!=(const T* const a, const UniquePtr<U>& b) noexcept
{
    return a != b.get();
}

template <typename T, typename U>
bool operator!=(const UniquePtr<T>& a, const U* const b) noexcept
{
    return a.get() != b;
}

template <typename T>
bool operator!=(const UniquePtr<T>& ptr, std::nullptr_t) noexcept
{
    return ptr.get() != nullptr;
}

template <typename T>
bool operator!=(std::nullptr_t, const UniquePtr<T>& ptr) noexcept
{
    return ptr.get() != nullptr;
}
}    // namespace Mooned
