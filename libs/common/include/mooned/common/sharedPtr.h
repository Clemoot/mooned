#pragma once

namespace Mooned
{
template <typename T>
class SharedPtr
{
    template <typename U>
    friend class SharedPtr;

public:
    using element_type_t = T;

    constexpr SharedPtr() noexcept;
    constexpr SharedPtr(T* ptr) noexcept;
    template <typename Derived, typename = std::enable_if_t<std::is_base_of_v<T, Derived>, int>>
    constexpr SharedPtr(Derived* ptr) noexcept;
    constexpr SharedPtr(SharedPtr<T>&& other) noexcept;
    template <typename Derived, typename = std::enable_if_t<std::is_base_of_v<T, Derived>, int>>
    constexpr SharedPtr(SharedPtr<Derived>&& other) noexcept;
    constexpr SharedPtr(const SharedPtr<T>& other) noexcept;
    template <typename Derived, typename = std::enable_if_t<std::is_base_of_v<T, Derived>, int>>
    constexpr SharedPtr(const SharedPtr<Derived>& other) noexcept;
    ~SharedPtr();

    T* get() noexcept;
    const T* get() const noexcept;
    void reset(T* ptr = nullptr) noexcept;
    void reset(std::nullptr_t) noexcept;

    T& operator*() { return *m_Ptr; }
    const T& operator*() const { return *m_Ptr; }
    T* operator->() { return m_Ptr; }
    const T* operator->() const { return m_Ptr; }

    operator bool() const noexcept { return m_Ptr != nullptr; }

    SharedPtr<T>& operator=(SharedPtr<T>&& other) noexcept;
    template <typename Derived, typename = std::enable_if_t<std::is_base_of_v<T, Derived>, int>>
    SharedPtr<T>& operator=(SharedPtr<Derived>&& other) noexcept;
    SharedPtr<T>& operator=(const SharedPtr<T>& other) noexcept;
    template <typename Derived, typename = std::enable_if_t<std::is_base_of_v<T, Derived>, int>>
    SharedPtr<T>& operator=(const SharedPtr<Derived>& other) noexcept;
    SharedPtr<T>& operator=(std::nullptr_t) noexcept;

private:
    T* m_Ptr;
    size_t* m_Count;

    void releaseCurrentOwnership() noexcept;
};

template <
    typename T,
    typename... ConstructorArgs,
    typename = std::enable_if_t<std::is_constructible_v<T, ConstructorArgs...>, int>>
SharedPtr<T> make_shared(ConstructorArgs&&... args) noexcept
{
    return SharedPtr<T>(new T(args...));
}

template <typename Derived, typename Base, typename = std::enable_if_t<std::is_base_of_v<Base, Derived>, int>>
SharedPtr<Derived> dynamic_cast_pointer(SharedPtr<Base>& ptr) noexcept
{
    return SharedPtr<Derived>(dynamic_cast<Derived*>(ptr.get()));
}

template <typename Derived, typename Base, typename = std::enable_if_t<std::is_base_of_v<Base, Derived>, int>>
SharedPtr<Derived> dynamic_cast_pointer(const SharedPtr<Base>& ptr) noexcept
{
    return SharedPtr<Derived>(dynamic_cast<const Derived*>(ptr.get()));
}

// ----------------

template <typename T>
constexpr SharedPtr<T>::SharedPtr() noexcept: m_Ptr(nullptr), m_Count(new size_t(1UL))
{
}

template <typename T>
constexpr SharedPtr<T>::SharedPtr(T* ptr) noexcept: m_Ptr(ptr), m_Count(new size_t(1UL))
{
}

template <typename T>
template <typename Derived, typename>
constexpr SharedPtr<T>::SharedPtr(Derived* ptr) noexcept: m_Ptr(ptr), m_Count(new size_t(1UL))
{
}

template <typename T>
constexpr SharedPtr<T>::SharedPtr(SharedPtr<T>&& other) noexcept: m_Ptr(other.m_Ptr), m_Count(other.m_Count)
{
    other.m_Ptr   = nullptr;
    other.m_Count = nullptr;
}

template <typename T>
template <typename Derived, typename>
constexpr SharedPtr<T>::SharedPtr(SharedPtr<Derived>&& other) noexcept: m_Ptr(other.m_Ptr), m_Count(other.m_Count)
{
    other.m_Ptr   = nullptr;
    other.m_Count = nullptr;
}

template <typename T>
constexpr SharedPtr<T>::SharedPtr(const SharedPtr<T>& other) noexcept: m_Ptr(other.m_Ptr), m_Count(other.m_Count)
{
    if (m_Count)
        (*m_Count)++;
}

template <typename T>
template <typename Derived, typename>
constexpr SharedPtr<T>::SharedPtr(const SharedPtr<Derived>& other) noexcept: m_Ptr(other.m_Ptr), m_Count(other.m_Count)
{
    if (m_Count)
        (*m_Count)++;
}

template <typename T>
SharedPtr<T>::~SharedPtr()
{
    releaseCurrentOwnership();
}

template <typename T>
T* SharedPtr<T>::get() noexcept
{
    return m_Ptr;
}

template <typename T>
const T* SharedPtr<T>::get() const noexcept
{
    return m_Ptr;
}

template <typename T>
void SharedPtr<T>::reset(T* ptr) noexcept
{
    releaseCurrentOwnership();
    m_Ptr   = ptr;
    m_Count = new size_t(1UL);
}

template <typename T>
void SharedPtr<T>::reset(std::nullptr_t) noexcept
{
    releaseCurrentOwnership();
    m_Ptr   = nullptr;
    m_Count = new size_t(1UL);
}

template <typename T>
SharedPtr<T>& SharedPtr<T>::operator=(SharedPtr<T>&& other) noexcept
{
    m_Ptr         = other.m_Ptr;
    m_Count       = other.m_Count;
    other.m_Ptr   = nullptr;
    other.m_Count = nullptr;
    return *this;
}

template <typename T>
template <typename Derived, typename>
SharedPtr<T>& SharedPtr<T>::operator=(SharedPtr<Derived>&& other) noexcept
{
    m_Ptr         = other.m_Ptr;
    m_Count       = other.m_Count;
    other.m_Ptr   = nullptr;
    other.m_Count = nullptr;
    return *this;
}

template <typename T>
SharedPtr<T>& SharedPtr<T>::operator=(const SharedPtr<T>& other) noexcept
{
    releaseCurrentOwnership();
    m_Ptr   = other.m_Ptr;
    m_Count = other.m_Count;
    if (m_Count)
        (*m_Count)++;
    return *this;
}

template <typename T>
template <typename Derived, typename>
SharedPtr<T>& SharedPtr<T>::operator=(const SharedPtr<Derived>& other) noexcept
{
    releaseCurrentOwnership();
    m_Ptr   = other.m_Ptr;
    m_Count = other.m_Count;
    if (m_Count)
        (*m_Count)++;
    return *this;
}

template <typename T>
SharedPtr<T>& SharedPtr<T>::operator=(std::nullptr_t) noexcept
{
    reset(nullptr);
    return *this;
}

template <typename T>
void SharedPtr<T>::releaseCurrentOwnership() noexcept
{
    if (!m_Count)
        return;
    (*m_Count)--;
    if (*m_Count == 0UL) {
        delete m_Ptr;
        delete m_Count;
        m_Ptr   = nullptr;
        m_Count = nullptr;
    }
}

template <typename T, typename U>
bool operator==(const SharedPtr<T>& a, const SharedPtr<U>& b) noexcept
{
    return a.get() == b.get();
}

template <typename T, typename U>
bool operator==(const T* const a, const SharedPtr<U>& b) noexcept
{
    return a == b.get();
}

template <typename T, typename U>
bool operator==(const SharedPtr<T>& a, const U* const b) noexcept
{
    return a.get() == b;
}

template <typename T>
bool operator==(const SharedPtr<T>& ptr, std::nullptr_t) noexcept
{
    return ptr.get() == nullptr;
}

template <typename T>
bool operator==(std::nullptr_t, const SharedPtr<T>& ptr) noexcept
{
    return ptr.get() == nullptr;
}

template <typename T, typename U>
bool operator!=(const SharedPtr<T>& a, const SharedPtr<U>& b) noexcept
{
    return a.get() != b.get();
}

template <typename T, typename U>
bool operator!=(const T* const a, const SharedPtr<U>& b) noexcept
{
    return a != b.get();
}

template <typename T, typename U>
bool operator!=(const SharedPtr<T>& a, const U* const b) noexcept
{
    return a.get() != b;
}

template <typename T>
bool operator!=(const SharedPtr<T>& ptr, std::nullptr_t) noexcept
{
    return ptr.get() != nullptr;
}

template <typename T>
bool operator!=(std::nullptr_t, const SharedPtr<T>& ptr) noexcept
{
    return ptr.get() != nullptr;
}
}    // namespace Mooned
