#include <gtest/gtest.h>

#include <mooned/common/uniquePtr.h>

using namespace Mooned;

TEST(UniquePtrTest, NullptrTest)
{
    UniquePtr<char> ptr1;
    EXPECT_FALSE(ptr1);
    EXPECT_EQ(ptr1.get(), nullptr);
    EXPECT_EQ(ptr1, nullptr);
    EXPECT_EQ(nullptr, ptr1);

    UniquePtr<char> ptr2(nullptr);
    EXPECT_FALSE(ptr2);
    EXPECT_EQ(ptr2.get(), nullptr);
    EXPECT_EQ(ptr2, nullptr);
    EXPECT_EQ(nullptr, ptr2);

    UniquePtr<char> ptr3 = UniquePtr<char>(nullptr);
    EXPECT_FALSE(ptr3);
    EXPECT_EQ(ptr3.get(), nullptr);
    EXPECT_EQ(ptr3, nullptr);
    EXPECT_EQ(nullptr, ptr3);

    UniquePtr<char> ptr4 = nullptr;
    EXPECT_FALSE(ptr4);
    EXPECT_EQ(ptr4.get(), nullptr);
    EXPECT_EQ(ptr4, nullptr);
    EXPECT_EQ(nullptr, ptr4);
}

TEST(UniquePtrTest, BasicManipulation)
{
    char* c = new char(1);
    UniquePtr<char> ptr1(c);
    ASSERT_TRUE(ptr1);
    EXPECT_EQ(*ptr1, *c);
    EXPECT_EQ(ptr1, c);
    EXPECT_EQ(c, ptr1);
    EXPECT_NE(ptr1, nullptr);
    EXPECT_NE(nullptr, ptr1);

    ptr1.reset();
    ASSERT_FALSE(ptr1);
    EXPECT_EQ(ptr1, nullptr);
    EXPECT_EQ(nullptr, ptr1);

    ptr1 = make_unique<char>(2);
    ASSERT_TRUE(ptr1);
    EXPECT_EQ(*ptr1, 2);
    EXPECT_NE(ptr1, nullptr);
    EXPECT_NE(nullptr, ptr1);

    UniquePtr<char> ptr2(std::move(ptr1));
    ASSERT_TRUE(ptr2);
    ASSERT_FALSE(ptr1);
    EXPECT_EQ(ptr1, nullptr);
    EXPECT_EQ(nullptr, ptr1);
    EXPECT_NE(ptr2, nullptr);
    EXPECT_NE(nullptr, ptr2);
    EXPECT_EQ(*ptr2, 2);
}

TEST(UniquePtrTest, ClassManipulation)
{
    class A
    {
    public:
        A(int a_): a(a_) {}
        virtual ~A() = default;
        int a;
    };

    class B : public A
    {
    public:
        B(int a_, const char* b_): A(a_), b(b_) {}
        const char* b;
    };

    static_assert(!std::is_constructible_v<UniquePtr<B>, UniquePtr<A>&&>);
    static_assert(!std::is_constructible_v<UniquePtr<B>, UniquePtr<A>>);
    static_assert(!std::is_constructible_v<UniquePtr<A>, const UniquePtr<B>&>);
    static_assert(std::is_constructible_v<UniquePtr<A>, UniquePtr<B>&&>);

    UniquePtr<A> ptr_a = make_unique<A>(2);
    UniquePtr<B> ptr_b = make_unique<B>(3, "this is b pointer");

    ASSERT_TRUE(ptr_a);
    EXPECT_NE(ptr_a, nullptr);
    EXPECT_NE(nullptr, ptr_a);
    ASSERT_TRUE(ptr_b);
    EXPECT_NE(ptr_b, nullptr);
    EXPECT_NE(nullptr, ptr_b);

    EXPECT_EQ(ptr_a->a, 2);
    EXPECT_EQ(ptr_b->a, 3);
    EXPECT_STREQ(ptr_b->b, "this is b pointer");

    ptr_a = std::move(ptr_b);
    ASSERT_TRUE(ptr_a);
    EXPECT_NE(ptr_a, nullptr);
    EXPECT_NE(nullptr, ptr_a);
    EXPECT_EQ(ptr_a->a, 3);

    UniquePtr<B>& ptr_ab1 = dynamic_cast_pointer<B>(ptr_a);
    ASSERT_TRUE(ptr_a);
    ASSERT_TRUE(ptr_ab1);
    EXPECT_NE(ptr_ab1, nullptr);
    EXPECT_NE(nullptr, ptr_ab1);
    EXPECT_EQ(ptr_ab1->a, 3);
    EXPECT_STREQ(ptr_ab1->b, "this is b pointer");

    ptr_a = make_unique<B>(4, "this is another b pointer");
    ASSERT_TRUE(ptr_ab1);
    EXPECT_NE(ptr_ab1, nullptr);
    EXPECT_NE(nullptr, ptr_ab1);
    EXPECT_EQ(ptr_ab1->a, 4);
    EXPECT_STREQ(ptr_ab1->b, "this is another b pointer");

    ptr_a = make_unique<A>(5);

    UniquePtr<B>& ptr_ab2 = dynamic_cast_pointer<B>(ptr_a);
    ASSERT_FALSE(ptr_ab2);
    EXPECT_EQ(ptr_ab2, nullptr);
    EXPECT_EQ(nullptr, ptr_ab2);
    ASSERT_TRUE(ptr_a);
}

TEST(UniquePtrTest, DeleteContainedPointer)
{
    class SetOnDelete
    {
    public:
        SetOnDelete(int& var_, int value_): var(var_), value(value_) {}
        ~SetOnDelete() { var = value; }

        int& var;
        int value;
    };

    int test_value   = 0;
    SetOnDelete* tmp = nullptr;
    {
        UniquePtr<SetOnDelete> ptr = make_unique<SetOnDelete>(test_value, 1);
    }
    EXPECT_EQ(test_value, 1);
    {
        UniquePtr<SetOnDelete> master_ptr = nullptr;
        {
            UniquePtr<SetOnDelete> ptr = make_unique<SetOnDelete>(test_value, 2);
            master_ptr                 = std::move(ptr);
        }
        EXPECT_EQ(test_value, 1);
    }
    EXPECT_EQ(test_value, 2);
    {
        UniquePtr<SetOnDelete> master_ptr = make_unique<SetOnDelete>(test_value, 3);
        {
            UniquePtr<SetOnDelete> ptr(std::move(master_ptr));
            EXPECT_EQ(test_value, 2);
            master_ptr = std::move(ptr);
        }
        EXPECT_EQ(test_value, 2);
        master_ptr.reset(new SetOnDelete(test_value, 4));
        EXPECT_EQ(test_value, 3);
        tmp = master_ptr.release();
    }
    EXPECT_EQ(test_value, 3);
    {
        UniquePtr<SetOnDelete> ptr(tmp);
        ptr.reset();
        EXPECT_EQ(test_value, 4);
    }
}