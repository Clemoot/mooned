#include <gtest/gtest.h>

#include <mooned/common/sharedPtr.h>

using namespace Mooned;

class SharedPtrTest : public testing::Test
{
protected:
    class A
    {
    public:
        A(int a_): a(a_) {}
        virtual ~A() = default;
        int a;
    };

    class B : public A
    {
    public:
        B(int a_, const char* b_): A(a_), b(b_) {}
        const char* b;
    };

    static_assert(!std::is_constructible_v<SharedPtr<B>, SharedPtr<A>&&>);
    static_assert(!std::is_constructible_v<SharedPtr<B>, SharedPtr<A>>);
    static_assert(std::is_constructible_v<SharedPtr<A>, const SharedPtr<B>&>);
    static_assert(std::is_constructible_v<SharedPtr<A>, SharedPtr<B>&&>);

    class SetOnDelete
    {
    public:
        SetOnDelete(int& var_, int value_): var(var_), value(value_) {}
        ~SetOnDelete() { var = value; }

        int& var;
        int value;
    };
};

TEST_F(SharedPtrTest, NullptrTest)
{
    SharedPtr<char> ptr1;
    EXPECT_FALSE(ptr1);
    EXPECT_EQ(ptr1.get(), nullptr);
    EXPECT_EQ(ptr1, nullptr);
    EXPECT_EQ(nullptr, ptr1);

    SharedPtr<char> ptr2(nullptr);
    EXPECT_FALSE(ptr2);
    EXPECT_EQ(ptr2.get(), nullptr);
    EXPECT_EQ(ptr2, nullptr);
    EXPECT_EQ(nullptr, ptr2);

    SharedPtr<char> ptr3 = SharedPtr<char>(nullptr);
    EXPECT_FALSE(ptr3);
    EXPECT_EQ(ptr3.get(), nullptr);
    EXPECT_EQ(ptr3, nullptr);
    EXPECT_EQ(nullptr, ptr3);

    SharedPtr<char> ptr4 = nullptr;
    EXPECT_FALSE(ptr4);
    EXPECT_EQ(ptr4.get(), nullptr);
    EXPECT_EQ(ptr4, nullptr);
    EXPECT_EQ(nullptr, ptr4);
}

TEST_F(SharedPtrTest, BasicManipulation)
{
    char* c = new char(1);
    SharedPtr<char> ptr1(c);
    ASSERT_TRUE(ptr1);
    EXPECT_EQ(*ptr1, *c);
    EXPECT_EQ(ptr1, c);
    EXPECT_EQ(c, ptr1);
    EXPECT_NE(ptr1, nullptr);
    EXPECT_NE(nullptr, ptr1);

    ptr1.reset();
    ASSERT_FALSE(ptr1);
    EXPECT_EQ(ptr1, nullptr);
    EXPECT_EQ(nullptr, ptr1);

    ptr1 = make_shared<char>(2);
    ASSERT_TRUE(ptr1);
    EXPECT_EQ(*ptr1, 2);
    EXPECT_NE(ptr1, nullptr);
    EXPECT_NE(nullptr, ptr1);

    SharedPtr<char> ptr2(std::move(ptr1));
    ASSERT_TRUE(ptr2);
    ASSERT_FALSE(ptr1);
    EXPECT_EQ(ptr1, nullptr);
    EXPECT_EQ(nullptr, ptr1);
    EXPECT_NE(ptr2, nullptr);
    EXPECT_NE(nullptr, ptr2);
    EXPECT_EQ(*ptr2, 2);
}

TEST_F(SharedPtrTest, ClassManipulation)
{
    SharedPtr<A> ptr_a = make_shared<A>(2);
    SharedPtr<B> ptr_b = make_shared<B>(3, "this is b pointer");

    B* raw_b_ptr = ptr_b.get();

    ASSERT_TRUE(ptr_a);
    ASSERT_TRUE(ptr_b);

    EXPECT_EQ(ptr_a->a, 2);
    EXPECT_EQ(ptr_b->a, 3);
    EXPECT_STREQ(ptr_b->b, "this is b pointer");

    ptr_a = std::move(ptr_b);
    ASSERT_TRUE(ptr_a);
    ASSERT_EQ(ptr_a, raw_b_ptr);
    EXPECT_EQ(ptr_a->a, 3);

    SharedPtr<B> ptr_ab1 = dynamic_cast_pointer<B>(ptr_a);
    ASSERT_TRUE(ptr_a);
    ASSERT_TRUE(ptr_ab1);
    EXPECT_NE(ptr_ab1, nullptr);
    EXPECT_NE(nullptr, ptr_ab1);
    EXPECT_EQ(ptr_ab1->a, 3);
    EXPECT_STREQ(ptr_ab1->b, "this is b pointer");

    ptr_a   = make_shared<B>(4, "this is another b pointer");
    ptr_ab1 = dynamic_cast_pointer<B>(ptr_a);
    ASSERT_TRUE(ptr_ab1);
    EXPECT_NE(ptr_ab1, nullptr);
    EXPECT_NE(nullptr, ptr_ab1);
    EXPECT_EQ(ptr_ab1->a, 4);
    EXPECT_STREQ(ptr_ab1->b, "this is another b pointer");

    ptr_a = make_shared<A>(5);

    SharedPtr<B> ptr_ab2 = dynamic_cast_pointer<B>(ptr_a);
    ASSERT_FALSE(ptr_ab2);
    EXPECT_EQ(ptr_ab2, nullptr);
    EXPECT_EQ(nullptr, ptr_ab2);
    ASSERT_TRUE(ptr_a);
}

TEST_F(SharedPtrTest, DeleteOnDestructionWithSinglePointer)
{
    int test_value = 0;
    {
        SharedPtr<SetOnDelete> ptr = make_shared<SetOnDelete>(test_value, 1);
    }
    ASSERT_EQ(test_value, 1);
}

TEST_F(SharedPtrTest, DeleteOnDestructionWithCopiedPointers)
{
    int test_value = 0;

    SetOnDelete* base_ptr = new SetOnDelete(test_value, 1);
    {
        SharedPtr<SetOnDelete> ptr1 = SharedPtr(base_ptr);

        {
            SharedPtr<SetOnDelete> ptr2 = ptr1;
            EXPECT_EQ(ptr2, base_ptr);
            {
                SharedPtr<SetOnDelete> ptr3 = ptr1;
                SharedPtr<SetOnDelete> ptr4 = ptr3;
                SharedPtr<SetOnDelete> ptr5 = ptr2;

                EXPECT_EQ(ptr3, base_ptr);
                EXPECT_EQ(ptr4, base_ptr);
                EXPECT_EQ(ptr5, base_ptr);

                EXPECT_EQ(ptr3, ptr1);
                EXPECT_EQ(ptr4, ptr1);
                EXPECT_EQ(ptr5, ptr1);
            }
        }
        EXPECT_EQ(test_value, 0);
    }
    EXPECT_EQ(test_value, 1);
}

TEST_F(SharedPtrTest, DeleteOnDestructionWithMovedPointers)
{
    int test_value = 0;

    SetOnDelete* base_ptr       = new SetOnDelete(test_value, 1);
    SharedPtr<SetOnDelete> ptr1 = base_ptr;
    {
        SharedPtr<SetOnDelete> ptr2 = std::move(ptr1);
        EXPECT_EQ(ptr2, base_ptr);
        EXPECT_EQ(ptr1, nullptr);
        ptr1 = std::move(ptr2);
        EXPECT_EQ(ptr2, nullptr);
    }
    EXPECT_EQ(ptr1, base_ptr);
    EXPECT_EQ(test_value, 0);
    {
        SharedPtr<SetOnDelete> ptr3 = std::move(ptr1);
        EXPECT_EQ(ptr3, base_ptr);
        EXPECT_EQ(ptr1, nullptr);
    }
    EXPECT_EQ(test_value, 1);
}

TEST_F(SharedPtrTest, DeleteOnReset)
{
    int test_value = 0;

    SharedPtr<SetOnDelete> ptr = make_shared<SetOnDelete>(test_value, 1);
    {
        SetOnDelete* base_ptr = new SetOnDelete(test_value, 2);
        ptr.reset(base_ptr);
        EXPECT_EQ(ptr, base_ptr);
    }
    EXPECT_EQ(test_value, 1);
    {
        ptr.reset(nullptr);
        EXPECT_EQ(ptr, nullptr);
    }
    EXPECT_EQ(test_value, 2);
}